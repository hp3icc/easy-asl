# Easy-ASL

<img src="https://gitlab.com/hp3icc/easy-asl/-/raw/main/img/allmon3.jpeg" width="700" height="500">

Easy-ASL is a project designed to facilitate the installation, configuration and start-up process of the allstarlink node, linked to Echolink and DMR.


## Getting started

Easy-ASL is compatible with the official allstarlink version Beta2 PC or Raspberry distributions.

If you do not have a previous installation of allstar on your computer, Easy-ASL will install it if your operating system is:

* Debian 10 version arm or X86_64

* Debian 11 version X86_64

* Debian 12 version arm64 X86_64 "ASL3"

## Install Allstar

You can download the official .iso or .ima images for pc vps or raspberry from the official AllstarLink project wiki.

https://wiki.allstarlink.org/wiki/Downloads

## Install Easy-ASL over Debian 10 ARM or X86_64 or AllStarLink Beta2 official Images



```
apt update -y
apt upgrade -y
apt install sudo curl -y
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/easy-asl/-/raw/main/install.sh)"
```

## Install Easy-ASL over PC or VPS with Debian 11 or 12 over arm64 or X86_64 (architecture armhf Debian 11 or 12 not supported)



```
apt update && apt-get upgrade -y
apt-get install sudo curl -y
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/easy-asl/-/raw/main/install.sh)"
```


## Download Easy-ASL Raspberry with ASL3 & allmon3 Dashboard :

If you have a Raspberry B3+ or P4, you can download the preconfigured Easy-ASL image with ASL3 bridge to EchoLink and DMR, start the download at the following Link:

* <p><a href="https://drive.google.com/u/0/uc?id=1LEt7u4h1ap_3Yi65jXgktIjDkMJtP9Jc&export=download&confirm=t&uuid=1LEt7u4h1ap_3Yi65jXgktIjDkMJtP9Jc" target="_blank">Descargar</a> image Raspberry&nbsp;</p>

Latest Raspberry image revision: Easy-ASL     Rev: 01/01/2024 

Raspberry OS Lite Based on Debian version 12 arm64 (bookworm) 19/11/2024 (update 14/12/2024) 

Linux kernel 6.6 

Dashboard: Allmon3 1.4.1

<!--
  hide
hide
--> 
## SSH or Terminal Login Raspberry Image

User: pi 

password: Panama507 
## Setup

To use Easy-ASL, you must run the command:

menu-asl

You can select 3 options.

<img src="https://gitlab.com/hp3icc/easy-asl/-/raw/main/img/IMG_3973.jpg" width="75" height="75">
<img src="https://gitlab.com/hp3icc/easy-asl/-/raw/main/img/IMG_3974.jpg" width="75" height="75">
<img src="https://gitlab.com/hp3icc/easy-asl/-/raw/main/img/IMG_3975.jpg" width="75" height="75">

Option 1 has a wizard to configure the Allstar node and Echolink node.

Option 2 has a wizard to configure the bridge to DMR networks

Option 3 is the official menu of the allstarlink project.

The configuration wizards, you only have to answer the questions with your information and at the end the wizard will make all the necessary configurations.

If you need to make any additional changes after the configuration, you can directly edit the files or use the wizard again.

## credits


Special thanks to Steve N4IRS, Claudio LU8ANB, Shane M0VUB, Francois F4JBM, for their guides in the allstar installation process


## sourse

* AllstarLink : https://www.allstarlink.org/

* Allmon3 : https://github.com/AllStarLink/Allmon3



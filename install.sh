#!/bin/bash
if [ -f "/opt/curl.txt" ]; then
  rm /opt/curl.txt
fi
if systemctl is-active "apache2" >/dev/null 2>&1; then
    systemctl stop apache2
    systemctl disable apache2
fi
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/ipv6off.sh)" &&
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/rpiswap.sh)" &&
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/asl-wizard.sh)" &&
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/nginx.sh)" 
if [ -d "/etc/asterisk" ]; then
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/app.sh)" &&
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/allmon3.sh)"
fi
if [ -d "/etc/allmon3" ]; then
systemctl stop allmon3
systemctl enable allmon3 
systemctl start allmon3
fi 
if [ -d "/etc/nginx" ]; then
  if [ ! -f "/etc/nginx/sites-available/000" ]; then
sudo cat > /etc/nginx/sites-available/000 <<- "EOFX1" 
server {
    listen wd0p;


    root w0d0;
    index index.php index.html index.htm;

    #server_name _;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php-fpm.sock;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }

    error_page 404 /404.html;
    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
        root /usr/share/nginx/html;
    }
}

EOFX1
  fi
fi
if [ -f "/etc/nginx/sites-enabled/default" ]; then
rm /etc/nginx/sites-enabled/default
fi
if [ -f "/etc/nginx/sites-enabled/000" ]; then
   rm /etc/nginx/sites-enabled/000
fi
if [ -f "/etc/nginx/sites-enabled/allmon3" ]; then
rm /etc/nginx/sites-enabled/allmon3
fi
sudo systemctl restart nginx
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-asl)" 
if [ -f "/bin/menu" ]; then
   bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu)"
fi
#if [ $aslr = 1 ]; then
cat > /tmp/completado.sh <<- "EOF"
#!/bin/bash
while : ; do
choix=$(whiptail --title "Script Proyect HP3ICC Esteban Mackay 73." --menu " Precione enter (return o intro) para finalizar la instalacion y reiniciar" 11 85 3 \
1 " Iniciar Reinicio de equipo " 3>&1 1>&2 2>&3)
exitstatus=$?
#on recupere ce choix
#exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "Your chosen option:" $choix
else
    echo "You chose cancel."; break;
fi
# case : action en fonction du choix
case $choix in
1)
sudo reboot
;;
esac
done
exit 0
EOF
#fi
sudo chmod +x /tmp/completado.sh
history -c && history -w
sh /tmp/completado.sh
